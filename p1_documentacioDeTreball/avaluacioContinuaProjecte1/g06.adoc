== Grup 06 (MAT PLACE -> mat-place)

****
* Aitor Burruezo Rodríguez
* Ivan Jurado Márquez 
* Pol Pallarès Giral
* Oriol Poncelas Crespo 
****

=== Setmana 2 (27/09/2021)

Tasques::

Creeu el projecte de git tal i com s'explica al document (Consideracions generals projecte):::
** Falta readme.adoc
Afegiu tots els integrants del grup i els professors al projecte:::
** ok
Planifiqueu les tasques a realitzar (fàcil, essencialment són les que us proposem aquesta setmana):::
** No fet
Assegureu-vos d'anar actualitzant la gestió personal de tasques i hores al fitxer *readme.adoc*:::
** No fet

Entregables::

Acta de la primera reunió:::
** Falta l'acceptació de la primera reunió. Els acords no em quadren.
Document dubtes i preguntes / preparació de la segona reunió amb el client:::
** ¿?
Catàleg de requisits (working version):::
** No és un catàleg. Us limiteu a la info de la reunió, s'ús escapen coses.

Valoració qualitativa::
** El seguiment puntua a totes les tasques i el vostre és un desastre. Llegiu amb detall el que es demana.


==== Setmana 3 (04/10/2021)

Tasques::

Planifiqueu les tasques a realitzar:::
No estan planificades. Poseu el que heu fet no la planificació. Heu d'indicar el número d'hores que penseu que tardareu a fer les tasques que planifiqueu (això serveix per veure com treballeu i si sou capaços de planificar correctament la feina que feu (normalment no l'encertareu, ja que és un aprenentatge empíric))
Assegureu-vos d'anar actualitzant la gestió personal de tasques i hores al fitxer *readme.adoc*:::
Fet
S'utilitza el gitlab com a repositori globla de l'aplicació:::
NO FET

Entregables::

catàleg de requisits (final version):::
* S'ha de revisar, feu-ne un repàs ja que en falten. 
* Repasseu la documentació. La pregunta 2 conté una resposta incorrecte. A les preguntes 6, 7 i 8 no és possible saber diferenciar entre la persona que entra dades a l'aplicació, els usuaris de les sales, l'usuari que fa la reserva....
* Per això és molt important el glossari de termes. Repasseu-lo, ja que és molt incomplet i no defineix correctament aquests termes.
Inclou la informació que es voldrà mantenir de manera no volàtil, poseu-la en forma de requisits.:::
Ho heu d'afinar una mica més, però barregeu la informació a punts diferents de la documentació.
cens d'actors amb els guions corresponents.  (final version):::
Fet
catàleg de casos d'ús amb les seqüencies associades(working version):::
Teniu la primera versió dels casos d'ús, ara cal explosionar-la i veure si els teniu tots.
glossari de conceptes (working version):::
Repasseu-ho. És incomplet, hi falten termes i no aclariu correctament la terminologia a utilitzar.
acta segona reunió (final version):::
Heu de solucionar tots els problemes que hem detectat.

==== Setmana 4 (11/10/2021)

Tasques::

Planifiqueu les tasques a realitzar:::
TODO
Assegureu-vos d'anar actualitzant la gestió personal de tasques i hores al fitxer *readme.adoc*:::
TODO

Entregables::

catàleg de requisits (final version):::
TODO
cens d'actors amb els guions corresponents.  (final version):::
TODO
catàleg de casos d'ús amb les seqüencies associades(final version):::
TODO
glossari de conceptes (final version):::
TODO
Prototipatge de les interfícies d'usuari (final version):::
TODO
Diagrames de casos d'ús (final version):::
TODO

Valoració qualitativa::

==== Setmana 5 (18/10/2021)

Tasques::

Planifiqueu les tasques a realitzar:::
TODO
Assegureu-vos d'anar actualitzant la gestió personal de tasques i hores al fitxer *readme.adoc*:::
TODO
Cal elaborar el document final d'especificació de requisits que haurà de contenir:::
** Propòsit i abast
** Descripció de la situació actual del "negoci"
** Definició del projecte
** Cens de requisits
** Cens d'actors
** Cens de casos d'ús
** Diagrames de casos d'ús
** Especificació dels casos d'ús
** Prototipatge de les interfícies d'usuari
** Conclusions
** Divisió de les tasques, opinions personals i avaluació personal de cada membre de l'equip.

Valoració qualitativa::